﻿using HypoportTask.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HypoportTask.Classes
{
    public class Zoo : IZoo
    {
        private List<IAnimal> animals;

        public Zoo()
        {
            animals = new List<IAnimal>();
            ZooStartupForSpecimens();
        }
        // Adding the animals into our zoo.
        private void ZooStartupForSpecimens()
        {
            for (int i = 0; i < 5; i++)
            {
                animals.Add(new Monkey());
            }

            for (int i = 0; i < 5; i++)
            {
                animals.Add(new Giraffe());
            }

            for (int i = 0; i < 5; i++)
            {
                animals.Add(new Elephant());
            }
        }
        // Decreasing the health of all animals in the zoo.
        public void SimulateHunger()
        {
            List<IAnimal> animalsToRemove = new List<IAnimal>();

            foreach (var animal in animals.ToList()) 
            {
                animal.HungerAttack();
                //Checks if the animal is alive. If not it gets added to a list of soon to be removed animals.
                if (!animal.IsAlive())
                {
                    animalsToRemove.Add(animal);
                }
               
            }
            // Removing the dead animals from the zoo.
            foreach (var animalToRemove in animalsToRemove)
            {
                animals.Remove(animalToRemove);
            }
        }
        // Getting all the animals that are still alive.
        public List<IAnimal> GetAnimals()
        {
            return animals;
        }
        //Method that increases the health of all the animals in the zoo.
        public void SimulateFeeding()
        {
            foreach (var animal in animals)
            {
                animal.SimulateFeeding();
            }
        }
        // Getting the number of alive animals in the zoo.
        public int GetAliveAnimalsCount()
        {
            int aliveAnimalsCounter = 0;
            foreach (var animal in animals)
            {
                if (animal.IsAlive())
                {
                    aliveAnimalsCounter++;
                }

            }
            return aliveAnimalsCounter;
        }
        // Adding new animal to the zoo. (Monkey, Elephant or Giraffe)
        public void AddAnimalToZoo(IAnimal animal)
        {
            animals.Add(animal);
        }
    }
}
