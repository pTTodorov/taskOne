﻿using HypoportTask.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HypoportTask.Classes
{
    public class Giraffe : Animal
    {
        // Method that checks if the giraffe is still alive.
        public override bool IsAlive()
        {
            if(Health >= 50)
            {
                return true;
            }
            return false;
            
        }
    }
}
