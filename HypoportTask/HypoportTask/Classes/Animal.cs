﻿using HypoportTask.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HypoportTask.Classes
{
    public abstract class Animal : IAnimal
    {
        public int Health {get;set;}
        public Animal()
        {
            Health = 100;
        }

        public virtual int HungerAttack()
        {
            int hunger = new Random().Next(0, 21);
            Health -= hunger;
            return Health;
        }

        public abstract bool IsAlive();

        public virtual int SimulateFeeding()
        {
            int feed = new Random().Next(10, 26);
            Health += feed;
            if (Health > 100)
            {
                Health = 100;
            }
            return Health;
        }
    }
}
