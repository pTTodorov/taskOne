﻿using HypoportTask.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HypoportTask.Classes
{
    public class Elephant : Animal
    {
        private bool CanWalk { get; set; } = true;
        private bool isElephantDead = false;

        // Method simulating the effects of hunger on the elephant's health (reduces its health by a random number 0-20).
        public override int HungerAttack()
        {
            int hunger = new Random().Next(0, 21);
            Health -= hunger;

            if (Health < 70 && !CanWalk)
            {
                isElephantDead = true;
            }
            else if (Health < 70)
            {
                CanWalk = false;
            }

            if (isElephantDead)
            {
                Health = 0;
                isElephantDead = true;
            }

            return Health;
        }

        // Method simulating the feeding of the elephant and increasing its health by a random number 10-25.
        public override int SimulateFeeding()
        {
            int feed = new Random().Next(10, 26);
            int healthBeforeFeeding = Health;
            Health += feed;

            if (Health > 100)
            {
                Health = 100;
            }
            if (healthBeforeFeeding <= 70 && !CanWalk && Health > 70)
            {
                CanWalk = true;
            }
            return Health;
        }
        //Method that checks if the animal is still alive.
        public override bool IsAlive()
        {
            if (isElephantDead == true && Health < 70)
            {
                return false;
            }
            return true;

        }
    }
}
