﻿using HypoportTask.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HypoportTask.Classes
{
    public class Monkey : Animal
    {
        // Method that checks if the monkey is alive.
        public override bool IsAlive()
        {
            if (Health >= 30)
                return true;
            return false;
        }
    }
}
