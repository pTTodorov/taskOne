﻿using HypoportTask.Classes;
using HypoportTask.Contracts;

class Program
{
    static void Main(string[] args)
    {
        Zoo zoo = new Zoo();
        Console.WriteLine("Welcome to my Simulated Zoo!");

        while (true)
        {
            Console.WriteLine("Menu:");
            Console.WriteLine("1. Simulate Hunger");
            Console.WriteLine("2. Simulate Feeding");
            Console.WriteLine("3. Add New Animal");
            Console.WriteLine("4. Exit");
            Console.Write("Enter your choice: ");

            if (int.TryParse(Console.ReadLine(), out int choice))
            {
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Simulating Hunger...");
                        zoo.SimulateHunger();
                        Console.WriteLine();
                        break;
                    case 2:
                        Console.WriteLine("Simulating Feeding...");
                        zoo.SimulateFeeding();
                        Console.WriteLine();
                        break;
                    case 3:
                        Console.Write("Enter the species of the new animal: ");
                        string specimen = Console.ReadLine();
                        if (specimen == "Giraffe"  )
                        {
                            IAnimal newAnimal = new Giraffe();
                            zoo.AddAnimalToZoo(newAnimal);
                            Console.WriteLine($"New {specimen} added to the zoo.");
                            Console.WriteLine();
                        }
                        else if(specimen == "Monkey")
                        {
                            IAnimal newAnimal = new Monkey();
                            zoo.AddAnimalToZoo(newAnimal);
                            Console.WriteLine($"New {specimen} added to the zoo.");
                            Console.WriteLine();
                        }
                        else if(specimen == "Elephant")
                        {
                            IAnimal newAnimal = new Elephant();
                            zoo.AddAnimalToZoo(newAnimal);
                            Console.WriteLine($"New {specimen} added to the zoo.");
                            Console.WriteLine();
                        }
                        else
                        {
                            Console.WriteLine("Only specimens you can add: Monkey, Giraffe or Elephant !");
                            Console.WriteLine();
                        }
                        break;
                    case 4:
                        Console.WriteLine("Exiting the program.");
                        return;
                    default:
                        Console.WriteLine("Invalid choice. Please enter a valid option.");
                        break;
                }
                Console.WriteLine($"Number of Animals Alive: {zoo.GetAliveAnimalsCount()}");
                foreach (var animal in zoo.GetAnimals())
                {
                    if (animal.GetType().Name.Equals("Elephant") && animal.Health<70)
                    {
                        Console.WriteLine($"{animal.GetType().Name} - {animal.Health} - Cannot Walk");
                    }
                    else
                    {
                        Console.WriteLine($"{animal.GetType().Name} - {animal.Health}");
                    }
                }

            }
            else
            {
                Console.WriteLine("Invalid input. Please enter a valid choice (1-4).");
            }
        }
    }
}