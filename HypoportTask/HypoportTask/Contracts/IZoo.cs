﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HypoportTask.Contracts
{
    public interface IZoo
    {
        void SimulateHunger();
        void SimulateFeeding();
        List<IAnimal> GetAnimals();
        int GetAliveAnimalsCount();
        void AddAnimalToZoo(IAnimal animal);
    }
}
