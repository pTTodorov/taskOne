﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HypoportTask.Contracts
{
    public interface IAnimal
    {
        int Health { get; set; }
        int HungerAttack();
        int SimulateFeeding();
        bool IsAlive();
    }
}
