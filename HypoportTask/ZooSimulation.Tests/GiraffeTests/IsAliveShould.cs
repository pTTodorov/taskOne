﻿using HypoportTask.Classes;
using HypoportTask.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.GiraffeTests
{
    [TestClass]
    public class IsAliveShould
    {
        [TestMethod]
        public void Giraffe_IsAlive_ReturnsTrueWhenHealthBiggerThan30()
        {
            // Arrange
            IAnimal giraffe = new Giraffe();

            // Act
            bool isAlive = giraffe.IsAlive();

            // Assert
            Assert.IsTrue(isAlive);
        }

        [TestMethod]
        public void Giraffe_IsAlive_ReturnsFalseWhenHealthLowerThan30()
        {
            // Arrange
            IAnimal giraffe = new Giraffe();
            giraffe.Health = 49;

            // Act
            bool isAlive = giraffe.IsAlive();

            // Assert
            Assert.IsFalse(isAlive);
        }
    }
}
