﻿using HypoportTask.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.AnimalTests
{
    [TestClass]
    public class SimulateFeedingShould
    {

        [TestMethod]
        public void SimulateFeeding_HealthDoesNotExceed100()
        {
            // Arrange
            var animal = new Mock<Animal>();

            // Act
            animal.Object.SimulateFeeding();

            // Assert
            Assert.AreEqual(100, animal.Object.Health);
        }
    }
}
