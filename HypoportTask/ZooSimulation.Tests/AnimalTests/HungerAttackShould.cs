﻿using HypoportTask.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.AnimalTests
{
    [TestClass]
    public class HungerAttackShould
    {
        [TestMethod]
        public void HungerAttack_DecreasesHealthWithinRange()
        {
            // Arrange
            var animal = new Mock<Animal>();

            // Act
            animal.Object.HungerAttack();

            // Assert
            Assert.IsTrue(animal.Object.Health <= 100);
        }
    }
}
