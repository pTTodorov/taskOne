﻿using HypoportTask.Classes;
using HypoportTask.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.MonkeyTests
{
    [TestClass]
    public class IsAliveShould
    {
        [TestMethod]
        public void Monkey_IsAlive_ReturnsTrueWhenHealthBiggerThan30()
        {
            // Arrange
            IAnimal monkey = new Monkey();

            // Act
            bool isAlive = monkey.IsAlive();

            // Assert
            Assert.IsTrue(isAlive);
        }

        [TestMethod]
        public void Monkey_IsAlive_ReturnsFalseWhenHealthLowerThan30()
        {
            // Arrange
            IAnimal monkey = new Monkey();
            monkey.Health = 29;

            // Act
            bool isAlive = monkey.IsAlive();

            // Assert
            Assert.IsFalse(isAlive);
        }
    }
}
