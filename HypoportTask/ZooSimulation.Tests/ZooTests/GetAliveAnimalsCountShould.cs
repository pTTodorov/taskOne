﻿using HypoportTask.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.ZooTests
{
    [TestClass]
    public class GetAliveAnimalsCountShould
    {
        [TestMethod]
        public void TestGetAliveAnimalsCount_ReturnsCorrectCount()
        {
            // Arrange
            Zoo zoo = new Zoo();

            // Act
            int aliveCount = zoo.GetAliveAnimalsCount();

            // Assert
            int expectedAliveCount = zoo.GetAnimals().Count;
            Assert.AreEqual(expectedAliveCount, aliveCount);
        }
    }
}
