﻿using HypoportTask.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.ZooTests
{
    [TestClass]
    public class SimulateHungerShould
    {
        [TestMethod]
        public void TestSimulateHunger_RemovesDeadAnimals()
        {
            // Arrange
            Zoo zoo = new Zoo();

            // Act
            zoo.SimulateHunger();
            zoo.SimulateHunger();
            zoo.SimulateHunger();
            zoo.SimulateHunger();
            zoo.SimulateHunger();
            zoo.SimulateHunger();
            zoo.SimulateHunger();

            // Assert
            Assert.AreNotEqual(15, zoo.GetAliveAnimalsCount());
        }
        [TestMethod]
        public void TestSimulateHunger_RemovesHealth()
        {
            // Arrange
            Zoo zoo = new Zoo();

            // Act
            zoo.SimulateHunger();

            // Assert
            foreach (var animal in zoo.GetAnimals())
            {
                Assert.IsTrue(animal.Health <= 100);
            }
        }
    }
}
