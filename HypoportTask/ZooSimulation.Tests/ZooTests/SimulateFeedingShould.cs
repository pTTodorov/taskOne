﻿using HypoportTask.Classes;
using HypoportTask.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.ZooTests
{
    [TestClass]
    public class SimulateFeedingShould
    {
        [TestMethod]
        public void TestSimulateFeeding_IncreasesAnimalHealth()
        {
            // Arrange
            Zoo zoo = new Zoo();

            // Act
            zoo.SimulateFeeding();

            // Assert
            foreach (var animal in zoo.GetAnimals())
            {
                Assert.IsTrue(animal.Health <= 100);
            }
        }
    }
}
