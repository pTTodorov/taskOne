﻿using HypoportTask.Classes;
using HypoportTask.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.ZooTests
{
    [TestClass]
    public class AddAnimalToZooShould
    {
        [TestMethod]
        public void TestAddAnimalToZoo_AddsAnimal()
        {
            // Arrange
            Zoo zoo = new Zoo();
            Mock<IAnimal> animal = new Mock<IAnimal>();

            // Act
            zoo.AddAnimalToZoo(animal.Object);

            // Assert
            int expectedCount = zoo.GetAnimals().Count;
            Assert.AreEqual(16, expectedCount); 
        }
    }
}
