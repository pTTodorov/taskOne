﻿using HypoportTask.Classes;
using HypoportTask.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.ElephantTests
{
    [TestClass]
    public class IsAliveShould
    {
        [TestMethod]
        public void Elephant_IsAlive_ShouldSucced_WhenHealthy()
        {
            // Arrange
            IAnimal elephant = new Elephant();

            // Act
            bool isAlive = elephant.IsAlive();

            // Assert
            Assert.IsTrue(isAlive);
        }
        [TestMethod]
        public void Elephant_IsAlive_ShouldSucceed_WhenHealthBelow70ButCanWalk()
        {
            // Arrange
            IAnimal elephant = new Elephant();
            elephant.Health = 70;
            elephant.HungerAttack();

            // Act
            bool isAlive = elephant.IsAlive();

            // Assert
            Assert.IsTrue(isAlive);
        }

        [TestMethod]
        public void Elephant_IsAlive_ShouldNotSucceed_WhenHealthBelow70AndCannotWalk()
        {
            // Arrange
            IAnimal elephant = new Elephant();
            elephant.Health = 70;
            elephant.HungerAttack();
            elephant.HungerAttack();

            // Act
            bool isAlive = elephant.IsAlive();

            // Assert
            Assert.IsFalse(isAlive);
        }
    }
}
