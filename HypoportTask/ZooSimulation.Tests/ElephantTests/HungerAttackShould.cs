﻿using HypoportTask.Classes;
using HypoportTask.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.ElephantTests
{
    [TestClass]
    public class HungerAttackShould
    {
        [TestMethod]
        public void Elephant_HungerAttack_ReducesHealth()
        {
            // Arrange
            IAnimal elephant = new Elephant();

            // Act
            int healthBeforeHungerAttack = elephant.Health;
            elephant.HungerAttack();
            elephant.HungerAttack();
            int newHealth = elephant.HungerAttack();

            // Assert
            Assert.IsTrue(healthBeforeHungerAttack > newHealth);
        }
    }
}
