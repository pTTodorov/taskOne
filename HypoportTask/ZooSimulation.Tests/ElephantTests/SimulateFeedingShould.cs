﻿using HypoportTask.Classes;
using HypoportTask.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulation.Tests.ElephantTests
{
    [TestClass]
    public class SimulateFeedingShould
    {
        [TestMethod]
        public void Elephant_SimulateFeeding_IncreasesHealthInRange()
        {
            // Arrange
            IAnimal elephant = new Elephant();
            elephant.Health = 72;
            // Act
            int healthBeforeFeeding = elephant.Health;
            int newHealth = elephant.SimulateFeeding();

            // Assert
            Assert.IsTrue(healthBeforeFeeding < newHealth);
        }
        [TestMethod]
        public void Elephant_SimulateFeeding_DoesNotIncreaseAbove100()
        {
            // Arrange
            IAnimal elephant = new Elephant();
            // Act
            int healthBeforeFeeding = elephant.Health;
            int newHealth = elephant.SimulateFeeding();

            // Assert
            Assert.IsTrue(healthBeforeFeeding == newHealth);
        }
    }
}
